
Local Quiz Notification plugin for Moodle
=========================================


Motivation for this plugin
--------------------------

In some organizations or some teaching scenarios, it is required to send manually notifications depending on the user grade of a specific quiz test result. There are already plugins out there, which provide a similar functionality. However this plugin is focussed on the following tasks:

1.  Display a list of current notifications and possibility to create new ones.
2.  Create, edit and delete notifications for specific course and quiz activity. 
3.  Display a list of enrolled users in a course, with the grade of the configured quiz activity (Possibility to sort by different fields)
4.  Possibility to select more than one user and send them the current notification.


Requirements
------------

This plugin requires Moodle 3.10

Installation
------------

Place the quiz_notification directory inside your Moodle's local directory (/local/quiz_notification).

See http://docs.moodle.org/en/Installing_plugins for details on installing Moodle plugins

Use
---
After install the plugin and browse to:

Site Administration / Plugins / Local plugins / Quiz Notification / Manage quiz notifications


Capabilities
------------
This plugin is used by administrator users with capability:

### moodle/site:config

By default, Moodle users are _not_ allowed to use the functionality provided by this plugin.
