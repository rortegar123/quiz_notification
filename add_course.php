<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add new course
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

require_once('../../config.php');

use core\output\notification;
use local_quiz_notification\form\add_course;
use local_quiz_notification\notifications_helper;

global $USER;

require_login();
$capability = has_capability('moodle/site:config', context_system::instance(), $USER);
if (!$capability) {
    throw new moodle_exception('permissiondenied', 'error', '', null);
}

$url = new moodle_url('/local/quiz_notification/add_course.php');
$context = context_system::instance();
$title = get_string('title_new_notification', 'local_quiz_notification');
$pageheading = get_string('head_new_notification', 'local_quiz_notification');
$urlconfig = new moodle_url($CFG->wwwroot . '/local/quiz_notification/config_notifications.php');

$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_heading($pageheading);
$PAGE->navbar->add( get_string('config_course_head', 'local_quiz_notification'), $urlconfig);
$PAGE->navbar->add($pageheading);
$PAGE->set_title($title);

$mform = new add_course(null, array());

if ($mform->is_cancelled()) {
    $message = get_string('msg_cancel_request', 'local_quiz_notification');
    redirect($urlconfig, $message, null, notification::NOTIFY_WARNING);

} else if ($mform->get_data()) {

    $data = $mform->get_data();
    $course = notifications_helper::get_course_by_id($data->courseid);

    if (!empty($course)) {
        notifications_helper::add_course($data);
        redirect(new moodle_url('/local/quiz_notification/add_notification.php', array('id' => $course->id)),
            get_string('msg_add_item_successfull', 'local_quiz_notification'), null, notification::NOTIFY_SUCCESS);

    } else {
        redirect($url,  get_string('msg_course_not_found', 'local_quiz_notification'), null, notification::NOTIFY_ERROR);
    }

} else {
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);
    $mform->display();
    echo $OUTPUT->footer();
}
