define(['jquery', 'core/ajax'], function ($, majax) {
    

    return { init: function () {    
       
        $('.quiz-notification table').on('click', '.detail-plus', function () {
            var loader = $('.quiz-notification').find('.loader');
            var notificationid = $(this).data("notificationid");
            quiz_notification_load_detail(notificationid, loader);            
        } );


        function quiz_notification_load_detail(id, loader) {
            $(loader).removeClass('hidden');

            majax.call([{
                methodname: 'local_quiz_notification_load_detail',
                args: { notificationid: id },
                done: function (response) {
                    $('.notification-modal').html(response.html);
                    $('#detailModal').modal('show');
                    $(loader).addClass('hidden');
                },
                fail: function (response) {
                    window.console.log(response);
                }
            }]);
        }

    }};
});