 
define(['jquery', 'jqueryui', 'core/ajax', 'core/config', 'local_quiz_notification/datatables'], function($) {
     
    return {
        init: function() {   
            $('#send-notification').DataTable({  
                order:  [[ 1, "desc" ]],
                searching:      true,
                paging:         false,
            });
            
            $('#config-notification').DataTable({  
                order:  [[ 5, "desc" ]],
                searching:      false,
                paging:         false,
            });
        }
    };
});

