<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add new course form
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */
namespace local_quiz_notification\form;


require_once($CFG->libdir.'/formslib.php');

use moodleform;

class add_course extends moodleform {

    function definition() {
        $mform = $this->_form; // Don't forget the underscore!

        $mform->addElement('html',  '<div class="quiz-notification">' );
        $mform->setType('html', PARAM_RAW);

        $mform->addElement('html', '<div class="alert alert-info" role="alert">'.
            get_string('msg_info_new_notification_form', 'local_quiz_notification').'</div>');
        $mform->setType('html', PARAM_RAW);

        $mform->addElement('text', 'courseid',  get_string('courseid', 'local_quiz_notification'));
        $mform->setType('courseid', PARAM_INT);

        $this->add_action_buttons(true, get_string('send', 'local_quiz_notification'));

        $mform->addElement('html',  '</div>' );
        $mform->setType('html', PARAM_RAW);

    }

    function validation($data, $files) {
        return array();
    }
}
