<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Add new notification Form
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

namespace local_quiz_notification\form;

use moodleform;
use local_quiz_notification\notifications_helper;

require_once($CFG->libdir.'/formslib.php');


class add_notification extends moodleform {

    function definition() {
        $mform = $this->_form; // Don't forget the underscore!
        $id = $this->_customdata['id'];

        $notification = notifications_helper::get_notification_by_id($id);
        $course = notifications_helper::get_course_by_id($notification->courseid);

        $mform->addElement('html',  '<div class="quiz-notification">' );
        $mform->setType('html', PARAM_RAW);

        $mform->addElement('html',  '<h4 class="title-form">'.
            get_string('course', 'local_quiz_notification').' '.$course->fullname.'</h4>' );
        $mform->setType('html', PARAM_RAW);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $id);

        $activities = notifications_helper::get_quiz_activities_by_courseid($course->id);
        $formatactivities = array();
        $formatactivities[0] = get_string('select', 'local_quiz_notification');
        foreach ($activities as $activity) {
            $formatactivities[$activity->id] = $activity->name;
        }
        $mform->addElement('select', 'quizid', get_string('quizid', 'local_quiz_notification'), $formatactivities);
        $mform->setDefault('quizid', $notification->quizid);

        $mform->addElement('text', 'subject',  get_string('subject', 'local_quiz_notification'));
        $mform->setType('subject', PARAM_TEXT);
        $mform->setDefault('subject', $notification->subject);

        $mform->addElement('editor', 'body', get_string('body', 'local_quiz_notification'))->setValue(
            array('text' => $notification->body));
        $mform->setType('body', PARAM_RAW);

        $this->add_action_buttons(true, get_string('send', 'local_quiz_notification'));

        $mform->addElement('html',  '</div>' );
        $mform->setType('html', PARAM_RAW);

    }


    function validation($data, $files) {
        return array();
    }
}
