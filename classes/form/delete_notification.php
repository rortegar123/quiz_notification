<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Delete notification
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

namespace local_quiz_notification\form;

use moodleform;
use local_quiz_notification\notifications_helper;

require_once($CFG->libdir.'/formslib.php');


class delete_notification extends moodleform {

    function definition() {
        $mform = $this->_form; // Don't forget the underscore!
        $id = $this->_customdata['id'];

        $notification = notifications_helper::get_notification_by_id($id);
        $course = notifications_helper::get_course_by_id($notification->courseid);

        $content = '<div class="quiz-notification"><div class="info-panel">';

        $content .= '<h3><i class="fas fa-trash-alt"></i> '.
            get_string('msg_delete_notification_confirm', 'local_quiz_notification').'</h3>';
        $content .= '<p><b>'. get_string('course', 'local_quiz_notification').': '.$course->fullname.'</b></p>';
        $content .= '<p><b>'. get_string('subject', 'local_quiz_notification').': </b>'.$notification->subject.'</p>';
        $content .= '<p><b>'. get_string('body', 'local_quiz_notification').': </b>'.$notification->body.'</p>';

        $mform->addElement('html', $content);
        $mform->setType('html', PARAM_RAW);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $id);

        $this->add_action_buttons(true, get_string('delete', 'local_quiz_notification'));

        $mform->addElement('html', '</div></div>');
        $mform->setType('html', PARAM_RAW);
    }


    function validation($data, $files) {
        return array();
    }
}
