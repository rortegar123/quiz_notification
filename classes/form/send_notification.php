<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Send notification
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

namespace local_quiz_notification\form;

use context_course;
use moodleform;

use local_quiz_notification\notifications_helper;

require_once($CFG->libdir.'/formslib.php');

class send_notification extends moodleform {

    function definition() {
        global $DB, $CFG;

        $mform = $this->_form; // Don't forget the underscore!
        $notificationid = $this->_customdata['id'];

        $notification = notifications_helper::get_notification_by_id($notificationid); // Get the given notification.

        $quiz = $DB->get_record('quiz', array('id' => $notification->quizid), '*', MUST_EXIST);
        $quizgrade = quiz_format_grade($quiz, $quiz->grade);
        $course = $DB->get_record('course', array('id' => $notification->courseid), '*', MUST_EXIST);

        $usernotifications = notifications_helper::get_all_user_notifications($notification->id);

        $htmlcontent = '<div class="quiz-notification"><div class="info-panel"><p><b>'.
            get_string('msg_info_send_notification', 'local_quiz_notification').'</b></p><p><b>'.
            get_string('course', 'local_quiz_notification').'</b>: '. $course->fullname .'('. $course->id .')</p><p><b>'.
            get_string('quiz', 'local_quiz_notification').'</b>: '. $quiz->name .'('. $quiz->id .')</p></div>'.
            '<table  class="generaltable" id="send-notification"><thead><th>'.
            get_string('username', 'local_quiz_notification').'</th><th>'.
            get_string('grade', 'local_quiz_notification').'</th><th>'.
            get_string('dategraded', 'local_quiz_notification').'</th><th>'.
            get_string('sent', 'local_quiz_notification').'</th></tr></thead><tbody>';

        $mform->addElement('html',  $htmlcontent );
        $mform->setType('html', PARAM_RAW);

        $students = get_enrolled_users(context_course::instance($course->id));
        foreach ($students as $user) {

            $usergrades = quiz_get_user_grades($quiz, $user->id);

            $dategraded = '';
            if (isset($usergrades[$user->id])) {
                $dategraded = $usergrades[$user->id]->dategraded;
                $dategraded = userdate($dategraded, '%d/%m/%Y', '99', false);
            }

            $userbestgrade = quiz_get_best_grade($quiz, $user->id);
            $userbestgrade = $userbestgrade * floatval($quizgrade) .'%';

            $htmlcontent = '<tr><td><a href="'.$CFG->wwwroot.'/user/profile.php?id='.$user->id.'">'.
                $user->firstname.' '.$user->lastname.'</a></td><td>'.$userbestgrade.'</td><td>'.
                $dategraded.'</td><td class="checkbox">';
            $mform->addElement('html',  $htmlcontent);
            $mform->setType('html', PARAM_RAW);

            if (isset($usernotifications[$user->id])) {
                $notdate = userdate($usernotifications[$user->id]->date, '%d/%m/%Y', '99', false);
                $mform->addElement('html',  '<i class="fas fa-check-circle"></i> '.
                    get_string('date', 'local_quiz_notification').': '.$notdate);
                $mform->setType('html', PARAM_RAW);

            } else {
                $mform->addElement('checkbox', 'user_'.$user->id, '',  '');
            }

            $htmlcontent = '</td></tr>';
            $mform->addElement('html',  $htmlcontent);
            $mform->setType('html', PARAM_RAW);
        }

        $mform->addElement('html',  '</tbody></table>');
        $mform->setType('html', PARAM_RAW);

        // Render buttons and hidden information.
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->setDefault('id', $notificationid);

        $this->add_action_buttons(true, get_string('send_notification', 'local_quiz_notification'));

        $mform->addElement('html',  '</div>' );
        $mform->setType('html', PARAM_RAW);

    }


    function validation($data, $files) {
        return array();
    }
}
