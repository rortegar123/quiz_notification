<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Notification helper. Contains all required functions
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

namespace local_quiz_notification;

use stdClass;


class notifications_helper {

    const QUIZ_NOTIFICATION_DBTABLE = 'local_quiz_notification';
    const QUIZ_NOTIFICATION_USER_DBTABLE = 'local_quiz_notification_user';


    /**
     * Add new notification to the database
     * @param $data
     */
    public static function add_course($data) {
        global $DB;

        $notification = new stdClass();
        $notification->courseid = $data->courseid;
        $notification->quizid = '';
        $notification->subject = '';
        $notification->body = '';
        $notification->date = time();

        $DB->insert_record(self::QUIZ_NOTIFICATION_DBTABLE, $notification);
    }


    /**
     * Add new notification to the database
     * @param $data
     */
    public static function update_notification($data) {
        global $DB;

        $notification = self::get_notification_by_id($data->id);
        $notification->quizid = $data->quizid;
        $notification->subject = $data->subject;
        $notification->body = $data->body["text"];

        $DB->update_record(self::QUIZ_NOTIFICATION_DBTABLE, $notification);
    }


    /**
     * Delete given notification from the database
     * @param $id
     */
    public static function delete_notification($id) {
        global $DB;

        $request = self::get_notification_by_id($id);
        if (!empty($request)) {
            $DB->delete_records(self::QUIZ_NOTIFICATION_DBTABLE, array('id' => $id ));
            return true;
        }

        return false;
    }


    /**
     * Returns the notification of the given id
     * @param $id
     */
    public static function get_notification_by_id($id) {
        global $DB;

        return $DB->get_record(self::QUIZ_NOTIFICATION_DBTABLE, array('id' => $id));
    }


    /**
     * Returns all the notifications
     */
    public static function get_all_notifications() {
        global $DB;

        return $DB->get_records(self::QUIZ_NOTIFICATION_DBTABLE);
    }


    /**
     * Returns record by quiz id
     */
    public static function get_quiz_by_id($id) {
        global $DB;

        return $DB->get_record('quiz', array('id' => $id));
    }


    /**
     * Returns the quiz activity by the given course id
     * @param $courseid
     */
    public static function get_quiz_activities_by_courseid ($courseid) {
        global $DB;

        return $DB->get_records('quiz', array('course' => $courseid));
    }

    /**
     * Get user by id from the user table
     * @param $id
     * @return object $user
     */
    public static function get_course_by_id ($id) {
        global $DB;

        return $DB->get_record('course', array('id' => $id));
    }


    /**
     * Returns all the notifications
     */
    public static function get_all_user_notifications($notificationid) {
        global $DB;

        return $DB->get_records(self::QUIZ_NOTIFICATION_USER_DBTABLE, array(
            'notificationid' => $notificationid), '', 'userid, date');
    }


    /**
     * Send emails with notifications to the given users, insert also the users in the db table
     * @param $data
     * @return bool
     */
    public static function send_notification_email($data) {

        $notification = self::get_notification_by_id($data->id);
        $message = $notification->body;
        $subject = $notification->subject;

        $from = \core_user::get_noreply_user();
        $from->firstname = get_string('email_username', 'local_quiz_notification');

        $error = false;
        foreach ($data as $key => $item) {
            if (strpos($key, 'user_') !== false) {

                $userid = str_replace('user_', '', $key);
                $user = self::get_user_by_id($userid);

                if (!empty($user)) {
                    $sendemail = email_to_user($user, $from, $subject, '', $message, '', '', true);

                    if ($sendemail) {
                        self::insert_notification_user($notification->id, $user->id);
                    } else {
                        $error = true;
                    }
                }
            }
        }

        return $error;
    }


    /**
     * Get user by id from the user table
     * @param $id
     * @return object $user
     */
    private static function get_user_by_id ($id) {
        global $DB;

        return $DB->get_record('user', array('id' => $id));
    }


    /**
     * Insert notification user
     * @param $notificationid
     * @param $userid
     */
    private static function insert_notification_user($notificationid, $userid) {
        global $DB;

        $request = new stdClass();
        $request->notificationid = $notificationid;
        $request->userid = $userid;
        $request->date = time();

        try {
            $DB->insert_record(self::QUIZ_NOTIFICATION_USER_DBTABLE, $request);

        } catch (\dml_exception $e) {
            // Return to the previous page.
            return false;
        }
    }
}
