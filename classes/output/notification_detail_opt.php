<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Notification Detail renderable
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

namespace local_quiz_notification\output;

use local_quiz_notification\notifications_helper;
use renderable;
use renderer_base;
use templatable;

class notification_detail_opt implements renderable, templatable {

    private $notificationid;


    public function __construct($notificationid) {
        $this->notificationid = $notificationid;
    }


    public function export_for_template(renderer_base $output) {
        global $CFG;

        $notification = notifications_helper::get_notification_by_id($this->notificationid);
        $quiz = notifications_helper::get_quiz_by_id($notification->quizid);
        $course = notifications_helper::get_course_by_id($notification->courseid);

        $notification->str_date = userdate($notification->date, '%d/%m/%Y', '99', false);
        if (!empty($course)) {
            $notification->coursename = $course->fullname;
        }
        if (!empty($quiz)) {
            $notification->quizname = $quiz->name; 
        }

        $data = new \stdClass();
        $data->notification = $notification;

        $data->wwwroot = $CFG->wwwroot;

        return $data;
    }
}
