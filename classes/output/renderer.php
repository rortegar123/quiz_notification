<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Notification renderer
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */


namespace local_quiz_notification\output;

defined('MOODLE_INTERNAL') || die();


use plugin_renderer_base;
use renderable;

class renderer extends plugin_renderer_base {


    /**
     * Main view page.
     * @param \templateable $page
     * @return string | boolean
     */
    public function render_notification_detail_opt(renderable $page) {
        $data = $page->export_for_template($this);
        return parent::render_from_template('local_quiz_notification/notification_detail_opt', $data);
    }
}
