<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Config notifications (List of all notifications created)
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

require_once('../../config.php');

use local_quiz_notification\notifications_helper;

global $USER, $DB;

require_login();
$capability = has_capability('moodle/site:config', context_system::instance(), $USER);
if (!$capability) {
    throw new moodle_exception('permissiondenied', 'error', '', null);
}

$context = context_system::instance();
$url = new moodle_url('/local/quiz_notification/config_notifications.php');

$PAGE->set_context($context);
$PAGE->set_url($url);
$pageheading = get_string('config_course_head', 'local_quiz_notification');
$PAGE->set_heading($pageheading);
$title = get_string('config_course_title', 'local_quiz_notification');
$PAGE->navbar->add($pageheading); // Bread Crum.
$PAGE->set_title($title);

$PAGE->requires->js_call_amd('local_quiz_notification/ajax_notification', 'init');
$PAGE->requires->js_call_amd('local_quiz_notification/local_datatables', 'init');

$data = array();

$notifications = notifications_helper::get_all_notifications();

foreach ($notifications as $notification) {

    $notification->body = substr(strip_tags($notification->body), 0, 200).'...';
    $notification->strdate = userdate($notification->date, '%d/%m/%Y', '99', false);

    $quiz = notifications_helper::get_quiz_by_id($notification->quizid);
    $quizname = '';
    if (!empty($quiz)) {
        $quizname = $quiz->name;
    }
    $notification->quizid = $quizname;

    $course = notifications_helper::get_course_by_id($notification->courseid);
    if (!empty($course)) {
        $notification->coursename = $course->fullname;
    }

    $data[] = $notification;
}


$isadmin = false;
if (has_capability('moodle/site:config', context_system::instance(), $USER)) {
    $isadmin = true;
}

$data = array('data' => $data,
    'is_admin' => $isadmin,
    'wwwroot' => $CFG->wwwroot,
);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo $OUTPUT->render_from_template('local_quiz_notification/config_notifications' , $data );

echo $OUTPUT->footer();
