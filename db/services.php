<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Notification renderer
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */


$services = array(
    'quiz_notification' => array(
        'functions' => array ('local_quiz_notification_load_detail'),
        'restrictedusers' => 0,
        'enabled' => 1,
        'shortname' => 'quiz_notification'
    )
);


$functions = array(
    'local_quiz_notification_load_detail' => array(
        'classname'   => 'local_quiz_notification_external',
        'methodname'  => 'load_notification_detail',
        'classpath'   => 'local/quiz_notification/externallib.php',
        'description' => 'Gets detail notification.',
        'type'        => 'read',
        'ajax'        => true,
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE, 'local_mobile')
    )
);
