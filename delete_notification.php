<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Delete notification
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */


require_once('../../config.php');

use core\output\notification;
use local_quiz_notification\form\delete_notification;
use local_quiz_notification\notifications_helper;

global $USER;

require_login();
if (!has_capability('moodle/site:config', context_system::instance(), $USER)) {
    throw new moodle_exception('permissiondenied', 'error', '', null);
}

// Get parameters.
$id = required_param('id', PARAM_INT);
$url = new moodle_url('/local/quiz_notification/delete_notification.php', array('id' => $id));
$context = context_system::instance();
$pageheading = get_string('title_delete_notification', 'local_quiz_notification');
$urlconfig = new moodle_url($CFG->wwwroot . '/local/quiz_notification/config_notifications.php');

$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_heading($pageheading);
$PAGE->navbar->add( get_string('config_course_head', 'local_quiz_notification'), $urlconfig);
$PAGE->navbar->add($pageheading);
$PAGE->set_title($pageheading);

$mform = new delete_notification(null, array('id' => $id ));
if ($mform->is_cancelled()) {
    redirect($urlconfig, get_string('msg_cancel_request', 'local_quiz_notification'), null, notification::NOTIFY_WARNING);

} else if ($mform->get_data()) {
    $data = $mform->get_data();
    notifications_helper::delete_notification($data->id);
    redirect($urlconfig, get_string('msg_delete_notification_successfull', 'local_quiz_notification'),
        null, notification::NOTIFY_SUCCESS);

} else {
    echo $OUTPUT->header();
    echo $OUTPUT->heading($pageheading);
    $mform->display();
    echo $OUTPUT->footer();
}
