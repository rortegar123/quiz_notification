<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Display detail information of the current notification
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

require_once($CFG->libdir . "/externallib.php");
use local_quiz_notification\output\notification_detail_opt;


class local_quiz_notification_external extends external_api {

    /**
     * Get notification detail params
     */
    public static function load_notification_detail_parameters() {

        $params = new external_function_parameters(
            array('notificationid' => new external_value(PARAM_TEXT, 'Notification ID', VALUE_REQUIRED))
        );

        return $params;
    }


    /**
     * Load notification detaul by ajax
     * @param $params
     * @return $html
     */
    public static function load_notification_detail($params) {
        global $PAGE;

        $PAGE->set_context(context_system::instance());
        $paramsvalidated = self::validate_parameters(self::load_notification_detail_parameters(), 
            array('notificationid' => $params));

        $renderer = $PAGE->get_renderer('local_quiz_notification');
        $notificationdetail = new notification_detail_opt($paramsvalidated['notificationid']);
        $content = $renderer->render($notificationdetail);

        $html = array('html' => $content);

        return $html;
    }


    /**
     * Returns detail notification
     */
    public static function  load_notification_detail_returns() {
        $result = new external_single_structure(
            array(
                'html' => new external_value(PARAM_RAW, 'Notification content html.'),
            )
        );

        return $result;
    }
}
