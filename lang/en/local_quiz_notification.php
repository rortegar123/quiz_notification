<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Translations
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

$string['pluginname'] = "Quiz Notification";

$string['email_username'] = "Test office";

$string['title_new_notification'] = "Create new notification for quiz activities";
$string['head_new_notification'] = "New notification";

$string['msg_cancel_request'] = "The request has been cancel";
$string['msg_add_item_successfull'] = "The new notification was created successfully";
$string['msg_course_not_found'] = "The course ID is not correct. Please try with a valid course id";

$string['config_course_head'] = "Manage quiz notifications";
$string['config_course_title'] = "Manage quiz activities notifications";
$string['msg_info_config_notifications'] = "The configured notifications will be send to the selected users in the detail page.";
$string['quiz_list_notification_title'] = "Enrolled users list";
$string['quiz_list_notification_head'] = "Enrolled users list";
$string['msg_send_notification_success'] = "All notifications has been send successfully. - Notification ID: ";
$string['msg_send_notification_error'] = "An SMTP error occurred while sending any of the notifications - Notification ID ";

$string['msg_info_new_notification_form'] = "To create a new notification, you first need to enter the Course ID.";
$string['additem'] = "New notification";

// Fields.
$string['courseid'] = "Course ID";
$string['course'] = "Course";
$string['quizid'] = "Quiz activity ID";
$string['quiz'] = "Quiz";
$string['body'] = "Email body";
$string['subject'] = "Email subject";
$string['list'] = "List of users";
$string['more'] = "More information";
$string['edit'] = "Edit";
$string['quizid_empty'] = "The quiz activity can not be empty";
$string['select'] = "Select... ";

$string['send'] = "Send";
$string['sent'] = "Sent";

$string['close'] = "Close";

$string['id'] = "ID";
$string['username'] = "Name";
$string['grade'] = "Grade";
$string['dategraded'] = "Grade date";
$string['date'] = "Date";

$string['send_notification'] = "Send notifications";
$string['msg_info_send_notification'] = "Notifications will be sent to selected users";

// Delete notification.
$string['msg_delete_notification_confirm'] = " Are you sure you want to delete the notification with the following data: ";
$string['title_delete_notification'] = "Delete notification ";
$string['msg_delete_notification_successfull'] = " The notification has been successfully deleted";
$string['delete'] = "Delete";

// Detail notification.
$string['title_detail_notification'] = "Notification";
$string['back'] = "Back";
