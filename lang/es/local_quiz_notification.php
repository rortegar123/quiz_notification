<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Translations ES
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */

$string['pluginname'] = "Quiz Notification";

$string['email_username'] = "Oficina Técnica GeLan";

$string['title_new_notification'] = "Crear nueva notificación para las actividades de tipo test";
$string['head_new_notification'] = "Nueva notificación";

$string['msg_cancel_request'] = "La solicitud ha sido cancelada";
$string['msg_add_item_successfull'] = "Nueva notificación añadida correctamente";
$string['msg_course_not_found'] = "No se ha podido crear la nueva notificación. El curso introducido no existe.";


$string['config_course_head'] = "Configurar notificaciones";
$string['config_course_title'] = "Configurar notificaciones para las actividades de tipo test";
$string['msg_info_config_notifications'] = "Configruación de las notificaciones que se enviará a los usuarios seleccionados de cada curso y actividad ";
$string['quiz_list_notification_title'] = "Listado de participantes en el curso";
$string['quiz_list_notification_head'] = "Listado de participantes";
$string['msg_send_notification_success'] = "Las notificaciones se han enviado correctamente - ID de la notificación: ";
$string['msg_send_notification_error'] = "Se ha producido un error SMTP al enviar alguna de las notificaciones - ID de la notificación: ";

$string['msg_info_new_notification_form'] = "Formulario para la creación de nuevas notificaciones que se enviará a los usuarios seleccionados de cada curso y actividad ";
$string['additem'] = "Nueva notificación";

// Fields.
$string['courseid'] = "ID Curso";
$string['course'] = "Curso";
$string['quizid'] = "ID Actividad 'Test'";
$string['quiz'] = "Actividad";
$string['body'] = "Texto del email / Cuerpo";
$string['subject'] = "Asunto del email";
$string['list'] = "Ver";
$string['edit'] = "Editar";
$string['quizid_empty'] = "El ID de la actividad no puede estar vacío";
$string['select'] = "Seleccione ... ";

$string['send'] = "Enviar";
$string['sent'] = "Enviado";
$string['id'] = "ID";
$string['username'] = "Nombre";
$string['grade'] = "Calificación";
$string['dategraded'] = "Fecha de la calificación";
$string['date'] = "Fecha";

$string['send_notification'] = "Enviar notificación";
$string['msg_info_send_notification'] = "Se enviarán notificaciones a los usuarios seleccionados";

// Delete notification.
$string['msg_delete_notification_confirm'] = "Está seguro de que desea borrar la notificación con los datos: ";
$string['title_delete_notification'] = "Borrar notificación";
$string['msg_delete_notification_successfull'] = "Su notificación se ha borrado correctamente";
$string['delete'] = "Borrar";

// Detail notification.
$string['title_detail_notification'] = "Notificación";
$string['back'] = "Volver";
