<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Send all notifications
 *
 * @package   local_quiz_notification
 * @copyright 2021 Raquel Ortega Rodriguez
 */


use core\output\notification;
use local_quiz_notification\form\send_notification;
use local_quiz_notification\notifications_helper;

require_once('../../config.php');
require_once($CFG->dirroot.'/grade/querylib.php');
require_once($CFG->dirroot . '/grade/lib.php');
require_once($CFG->dirroot . '/mod/quiz/locallib.php');
global $USER, $DB;

require_login();

$capability = has_capability('moodle/site:config', context_system::instance(), $USER);
if (!$capability) {
    throw new moodle_exception('permissiondenied', 'error', '', null);
}

// Get parameters.
$notificationid = required_param('id', PARAM_INT);

$url = new moodle_url('/local/quiz_notification/send_notification.php', array('id' => $notificationid));
$title = get_string('quiz_list_notification_title', 'local_quiz_notification');
$pageheading = get_string('quiz_list_notification_head', 'local_quiz_notification');

$PAGE->set_context(context_system::instance());
$PAGE->set_url($url);
$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->navbar->add( get_string('config_course_head', 'local_quiz_notification'),
    new moodle_url($CFG->wwwroot . '/local/quiz_notification/config_notifications.php'));
$PAGE->navbar->add($title);

$PAGE->requires->js_call_amd('local_quiz_notification/local_datatables', 'init');

$urlback = new moodle_url('/local/quiz_notification/config_notifications.php');
$mform = new send_notification(null, array('id' => $notificationid ));

if ($mform->is_cancelled()) {
    $message = get_string('msg_cancel_request', 'local_quiz_notification');
    redirect($urlback, $message, null, notification::NOTIFY_WARNING);

} else if ($mform->get_data()) {
    $data = $mform->get_data();

    $error = notifications_helper::send_notification_email($data);
    if ($error) {
        $message = get_string('msg_send_notification_error', 'local_quiz_notification').$notificationid;
        redirect($urlback, $message, null, notification::NOTIFY_ERROR);

    } else {
        $message = get_string('msg_send_notification_success', 'local_quiz_notification').$notificationid;
        redirect($urlback, $message, null, notification::NOTIFY_SUCCESS);
    }
}

// Print page.
echo $OUTPUT->header();
echo $OUTPUT->heading($title);
$mform->set_data(array('id' => $notificationid ));
$mform->display();
echo $OUTPUT->footer();
